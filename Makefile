CC = g++
CFLAGS = -Wall -Iinclude

prog: clean main.o App.o Diary.o
	$(CC) -o build/Diary src/*.o
clean:
	rm -f src/*.o
main.o: 
	$(CC) -o src/main.o -c src/main.cpp $(CFLAGS)
App.o:
	$(CC) -o src/App.o -c src/App.cpp $(CFLAGS)
Diary.o:
	$(CC) -o src/Diary.o -c src/Diary.cpp $(CFLAGS)